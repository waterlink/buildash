# buildash

Project: **bashbuilder**

## Installation

```
$ curl -L getbuildash.bitbucket.org | sudo bash
```

```
$ wget -q -O - getbuildash.bitbucket.org | sudo bash
```

```
$ wget -q https://bitbucket.org/bashbuilder/buildash/raw/stable/install -O - | sudo bash
```

## Usage

```
$ buildash version
buildash version 0.0.1
```

```
$ buildash pm install php
[buildash::INFO] Installing official builder bashbuilder/php
$ tree .
.
└── bashbuilder-php
    └── buildfile
	└── library
		└── pecl.sh

2 directories, 2 files
```

Warning: it might be required to run ```bash --login``` to be able to run buildash

## Examples

You will find example builders in examples folder of this project.

## For developers

### Installation of staging version

To check some feature or bugfix, you'll need to merge your {issue_number}_super_killer_feature_branch or {issue_number}_killer_bug_hotfix_branch into staging.

Then push it to bitbucket.

Next you'll need use a bit different command to install this staging version of buildash:

```
$ wget -q https://bitbucket.org/bashbuilder/buildash/raw/stable/install-staging -O - | sudo bash
```

### Instructions on using testing environment (only for insiders)

Just login on ssh user@test-buildash and fire up root console by:

```
$ ssh user@test-buildash
user@test-buildash password:     # you know what enter here if you are one from us :)

user@test-buildash:~$ sudo -s
[sudo] password for user:

root@test-buildash:~$ bash --login    # required for buildash to be sourced into bash session

root@test-buildash:~$ buildash test      # just check, if everything is ok, if not then run install command, mentioned in previous section
Buildash greets you
My binary is /opt/bashbuilder/buildash/bin/buildash-test
My prefix is /opt/bashbuilder/buildash
My version is 0.0.1
 							# you'll see something like this

```

