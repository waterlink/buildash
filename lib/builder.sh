#!/bin/bash

# builder library

. ${bash_builder_prefix}/library/library

function maintainer {
	echo Powered by "$@"
}

function environ {
	key=$1
	shift
	eval export $key="\"$@\""
}

function package-update {
	sudo apt-get update
	# WTF??
	# sudo apt-get upgrade -yy -q -f $@;
}

function package-install {
	# usage package-install name1 [ name2 ...]
	sudo apt-get install -yy -q -f $@
}

function unpack {
	echo unpacking $1
	# usage: unpack archive_name dir_save_name
	mkdir $2	
	if [[ "$1" == *tar ]]
		then tar -xvf $1 -C $2
	elif [[ "$1" == *tar.gz ]]
		then tar -xzvf $1 -C $2
	elif [[ "$1" == *tar.bz2 ]]
		then tar -xjvf $1 -C $2
	else echo "Unknown archive type..."
	fi
}

function download {
	# usage: download url
	timestamp=`date +%s`
	environ DIR_SAVE_NAME "/tmp/buildash$RANDOM$timestamp"
	mkdir -p $DIR_SAVE_NAME
	# wget -P $DIR_SAVE_NAME $1
	wget -O $DIR_SAVE_NAME/$2 $1
	unpack $DIR_SAVE_NAME/* $DIR_SAVE_NAME/unpacked
}

function install {
	# usage install config
	here=$PWD
	cd $DIR_SAVE_NAME/unpacked/*

	./configure "$@"
	make
	make install
	
	cd $here
}

function add {
	dstp=$(dirname $2)
	mkdir -p $dstp
	cp -f $1 $2
}

function clean {
	rm -rf $DIR_SAVE_NAME
}

function run {
	$@
}

function template {
	cat "templates/$1" | envsubst > "$2"
}
